package com.xmas.tree;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.xmas.messages.Messages;
import com.xmas.validation.ValidateDirection;
import com.xmas.validation.ValidateHeight;

public class XmasTreeCreator {
	
	public void createXmasTree() {
		
		Scanner scanner = new Scanner(System.in);
		
		String direction = getDirection(scanner);
		int height = getHeight(scanner);
		boolean inputIsCorrect = validateInput(height, direction);
		
		
		if(inputIsCorrect) {
			XmasTree xmasTree = new XmasTree(direction, height);
			XmasTreePrinter xmasTreePrinter = new XmasTreePrinter(xmasTree);
			
			xmasTreePrinter.printTree();
		}else{
			System.out.println(Messages.INFORM_ABOUT_WRONG_INPUT);
		}
		
		scanner.close();
	}
	
	private String getDirection(Scanner scanner){
		System.out.print(Messages.ASK_ABOUT_DIRECTION);
		String direction = scanner.nextLine().toUpperCase();
		
		return direction;
	}
	
	private int getHeight(Scanner scanner) {
		System.out.print(Messages.ASK_ABOUT_HEIGHT);	
		int height = 0;
		
		try {
			height = scanner.nextInt();
		}catch(InputMismatchException ime) {
			return -1;
		}
		
		return height;
	}
	
	private boolean validateInput(int height, String direction) {
		ValidateDirection validateDirection = new ValidateDirection();
		ValidateHeight validateHeight = new ValidateHeight();
		
		if(validateDirection.isDirection(direction) && validateHeight.isHeight(height)) {
			return true;
		}else {
			return false;
		}
	}
}
