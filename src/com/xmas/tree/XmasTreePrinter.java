package com.xmas.tree;

public class XmasTreePrinter implements TreePrinter{
	
	private XmasTree xmasTree;
	
	public XmasTreePrinter(XmasTree xmasTree) {
		this.xmasTree = xmasTree;
	}

	public void printTree() {
		switch(this.xmasTree.getDirection()) {
			case "UP":
				printUpTree(xmasTree);
				break;	
			case "DOWN":
				printDownTree(xmasTree);
				break;		
			case "LEFT":
				printLeftTree(xmasTree);
				break;	
			case "RIGHT":
				printRightTree(xmasTree);
				break;		
		}		
	}
	
	private void printUpTree(XmasTree xmasTree) {
		System.out.println("");
		for(int i=0; i<this.xmasTree.getHeight(); i++) {
			for(int j=0; j<this.xmasTree.getHeight()-i-1; j++) {
				System.out.print(" ");
			}
			
			for(int j=0; j<(i*2)+1; j++) {
				System.out.print("*");
			}
			
			System.out.println("");
		}
	}
	
	private void printDownTree(XmasTree xmasTree) {
		System.out.println("");
		for(int i=this.xmasTree.getHeight()-1; i>=0; i--) {
			for(int j=0; j<this.xmasTree.getHeight()-i-1; j++) {
				System.out.print(" ");
			}
			
			for(int j=0; j<(i*2)+1; j++) {
				System.out.print("*");
			}
			
			System.out.println("");
		}
	}
	
	private void printLeftTree(XmasTree xmasTree) {
		System.out.println("");
		for(int i=0; i<this.xmasTree.getHeight(); i++) {
			for(int j=0; j<this.xmasTree.getHeight()-i-1; j++) {
				System.out.print(" ");
			}
			
			for(int j=0;j<i+1;j++) {
				System.out.print("*");
			}
			
			System.out.println("");
		}
		
		for(int i=this.xmasTree.getHeight(); i>1; i--) {
			for(int j=0; j<this.xmasTree.getHeight()-i+1; j++) {
				System.out.print(" ");
			}
			
			for(int j=0;j<i-1;j++) {
				System.out.print("*");
			}
			
			System.out.println("");
		}
	}
	
	private void printRightTree(XmasTree xmasTree) {
		System.out.println("");
		for(int i=this.xmasTree.getHeight(); i>0; i--) {
			for(int j=0; j<this.xmasTree.getHeight()-i+1; j++) {
				System.out.print("*");
			}
			
			for(int j=0;j<i-1;j++) {
				System.out.print(" ");
			}
			
			System.out.println("");
		}
		
		for(int i=0; i<this.xmasTree.getHeight(); i++) {
			for(int j=0; j<this.xmasTree.getHeight()-i-1; j++) {
				System.out.print("*");
			}
			
			for(int j=0;j<i+1;j++) {
				System.out.print(" ");
			}
			
			System.out.println("");
		}
	}

	public XmasTree getXmasTree() {
		return xmasTree;
	}

	public void setXmasTree(XmasTree xmasTree) {
		this.xmasTree = xmasTree;
	}
}
