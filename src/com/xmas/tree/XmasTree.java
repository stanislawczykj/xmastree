package com.xmas.tree;

public class XmasTree {

	private String direction;
	private int height;
	
	public XmasTree(String direction, int height) {
		this.direction = direction;
		this.height = height;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
}
