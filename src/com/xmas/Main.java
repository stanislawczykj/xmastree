package com.xmas;

import com.xmas.tree.XmasTreeCreator;

public class Main {

	public static void main(String[] args) {
		
		XmasTreeCreator xmasTreeCreator = new XmasTreeCreator();
		
		xmasTreeCreator.createXmasTree();
	}
	
}
