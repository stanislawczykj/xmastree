package com.xmas.validation;

public class ValidateDirection {
	public boolean isDirection(String direction) {
		switch(direction) {
			case "UP": 
				return true;
			case "DOWN": 
				return true;
			case "LEFT": 
				return true;
			case "RIGHT": 
				return true;
			default:
				return false;
		}
	}
}
