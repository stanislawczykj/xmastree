package com.xmas.messages;

public class Messages {
	public static String ASK_ABOUT_DIRECTION = "Podaj kierunek drzewka (UP, DOWN, LEFT, RIGHT): ";
	public static String ASK_ABOUT_HEIGHT = "Podaj wysokosc drzewka: ";
	public static String INFORM_ABOUT_WRONG_INPUT = "Bledne dane";
}
